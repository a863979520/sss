<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../common/head.jsp"%>
<title>提示信息</title>
</style>
</head>
<body>
	<div style="text-align:center">
		<button type="button" class="btn btn-lg btn-info"
			onclick="window.location.href='${pageContext.request.contextPath}/'">${ sessionScope.msg}
		</button>
	</div>
</body>
</html>