<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../common/head.jsp"%>
<title>用户注册</title>
<style>
	.center{
		text-align:center;
	}
</style>
</head>
<body>
	<div class="jumbotron" style="text-align:center">
	  <div class="container">
	  	<h1>${ sessionScope.msg}，欢迎用户继续添加属性</h1>
	  </div>
	</div>
	<form action="${pageContext.request.contextPath}/reader/updateMessage" method="post">
		<div class="input-group input-group-lg text-center" style="margin: 0px auto;display: table;">
		账号<br><br>
			<input type="number" name="code" class="form-control center" placeholder="账号" value="${ sessionScope.code}" readonly=“readonly”>
		</div><br>
		<div class="input-group input-group-lg text-center" style="margin: 0px auto;display: table;">
		密码<br><br>
			<input type="text" name="password" class="form-control center" placeholder="密码" value="${ sessionScope.pwd}" aria-describedby="sizing-addon1">
		</div><br>
		<div class="input-group input-group-lg text-center" style="margin: 0px auto;display: table;">
		用户名<br><br>
			<input type="text" name="name" class="form-control center" placeholder="用户名" aria-describedby="sizing-addon1">
		</div><br>
		<div class="input-group input-group-lg text-center" style="margin: 0px auto;display: table;">
		地址<br><br>
			<input type="text" name="addres" class="form-control center" placeholder="地址" aria-describedby="sizing-addon1">
		</div><br>
		<div class="input-group input-group-lg text-center" style="margin: 0px auto;display: table;">
		爱好<br><br>
			<input type="text" name="hobby" class="form-control center" placeholder="爱好" aria-describedby="sizing-addon1">
		</div><br><br>
		<div class="input-group input-group-lg" style="margin: 0px auto;display: table;">
		<button class="btn btn-primary btn-block" type="submit">注册</button>
		</div>
	</form>
	
</body>
</html>