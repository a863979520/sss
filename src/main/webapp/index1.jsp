<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>
	<%@include file="./WEB-INF/common/jsp/head.jsp"%>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>用户登录</title>
    <style>
            /*web background*/
            .container{
                display:table;
                height:100%;
            }

            .row{
                display: table-cell;
                vertical-align: middle;
            }
            /* centered columns styles */
            .row-centered {
                text-align:center;
            }
            .col-centered {
                display:inline-block;
                float:none;
                text-align:left;
                margin-right:-4px;
            }
        </style>
    
  </head>
  <body>
    <div class="container">
    	<div class="row row-centered">
    		<div class="well col-md-6 col-centered">
    			<h2>欢迎登录</h2>
      			<form action="${pageContext.request.contextPath}/reader/login" method="post">
      				<div class="input-group input-group-md">
                            <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
                            <input type="number" class="form-control" id="code" name="code" placeholder="请输入用户账号(仅限数字)"/>
                        </div>
                        <div class="input-group input-group-md">
                            <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" id="password" name="password" placeholder="请输入密码"/>
                        </div>
                        <br/>
				          <label>
				            <input type="checkbox" value="remember-me"> Remember me
				          </label>
        				<button class="btn btn-success btn-block" type="submit">登录</button>
        				<input  class="btn btn-lg btn-primary btn-block" type="submit" formaction="${pageContext.request.contextPath}/reader/register" value="注册" />
      			</form>
      		</div>
      	</div>
      </div>

</html>