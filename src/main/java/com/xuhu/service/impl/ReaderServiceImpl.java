package com.xuhu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xuhu.mapper.ReaderMapper;
import com.xuhu.pojo.Reader;
import com.xuhu.service.ReaderService;

@Service
public class ReaderServiceImpl implements ReaderService{

	@Autowired
	private ReaderMapper ReaderMapper;
	@Override
	public Reader getpwdbycode(String code) {
		Reader readerMe = ReaderMapper.getpwdbycode(code);
		if(readerMe != null) {
			return readerMe;
		}else {
			return null;
		}
	}
	@Override
	public int insertReadCode(String code, String pwd) {
		int readerCode = ReaderMapper.insertReadCode(code,pwd);
		return readerCode;
	}
	public String getCode(String code) {
		String readerCode = ReaderMapper.getCode(code);
		return readerCode;
	}
	public int updateReaderMessage(Reader reader) {
		int updateR = ReaderMapper.updateReaderMessage(reader);
		return updateR;
	}

}
