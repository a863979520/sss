package com.xuhu.service;

import com.xuhu.pojo.Reader;

public interface ReaderService {

	Reader getpwdbycode(String code);
	int insertReadCode(String code, String pwd);
	int updateReaderMessage(Reader reader);
}
