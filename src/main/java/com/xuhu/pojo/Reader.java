package com.xuhu.pojo;

/**
 * 读者实体
 * @author 000
 *
 */
public class Reader {

	private int id;
	private String code;
	private String password;
	private String name;
	private String addres;
	private String hobby;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddres() {
		return addres;
	}
	public void setAddres(String addres) {
		this.addres = addres;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	@Override
	public String toString() {
		return "Reader [id=" + id + ", code=" + code + ", password=" + password + ", name=" + name + ", addres="
				+ addres + ", hobby=" + hobby + "]";
	}

}
