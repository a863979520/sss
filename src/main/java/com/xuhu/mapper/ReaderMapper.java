package com.xuhu.mapper;

import org.apache.ibatis.annotations.Param;

import com.xuhu.pojo.Reader;

public interface ReaderMapper {

	Reader getpwdbycode(String code);

	int insertReadCode(@Param("code")String code, @Param("pwd") String pwd);

	String getCode(String code);

	int updateReaderMessage(Reader reader);
}
